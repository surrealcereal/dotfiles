// Overrides for arkenfox' user.js, use latest version of that and updater.sh to apply overwrites.

// Disable Safe Browsing, this phones home to Google.
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);

//Reenable search engines
user_pref("keyword.enabled", true);
// Disk caching, which might improve performance if enabled.
user_pref("browser.cache.disk.enable", true);
// Enable favicons, the icons in bookmarks
user_pref("browser.shell.shortcutFavicons", true);

// Using extensions to manage cookie lifetime, hand control to me rather than deleting cookies on exit
user_pref("network.cookie.lifetimePolicy", 0);

// Change clear-on-shutdown policy
user_pref("privacy.clearOnShutdown.history", false);
user_pref("privacy.clearOnShutdown.cookies", false);

// Reenable history
user_pref("places.history.enabled", true);

// Firefox stores passwords in plain text and obsolete if you use a password manager.
// Mozilla also told people to stop using their password manager.
user_pref("signon.rememberSignons", false);
// Disable Pocket, it's proprietary trash
user_pref("extensions.pocket.enabled", false);
// Disable Mozilla account
user_pref("identity.fxaccounts.enabled", false);

// Disable letterboxing, it compromises on privacy and is an aesthetic choice
user_pref("privacy.resistFingerprinting.letterboxing", false);

// ----------------------------------------------

// Changes to firefox' user.js, not related to arkenfox
user_pref("browser.fullscreen.autohide", "false");

// Enable bookmarks on fullscreen, see https://askubuntu.com/questions/639206/firefox-show-bookmark-toolbar-in-fullscreen-mode/1203372#1203372
user_pref("toolkilt.legacyUserProfileCustomizations.stylesheets", "true");

// Disable restore session, because a WM's kill window is considered a crash by Firefox, making it show up every time Firefox is ended
user_pref("browser.sessionstore.resume_from_crash", "false");

// Disable Quick Find
user_pref("accessibility.typeaheadfind.manual", "false");
user_pref("accessibility.typeaheadfind", "false");
user_pref("accessibility.typeaheadfind.autostart", "false");
