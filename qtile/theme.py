import os

wallpaper = os.path.expanduser("~/.config/assets/currentwallpaper") # symlink

colors = {
    "background": "#1f1f1f",
    "text": "#5b5b5b",
    "transparent": "#00000000" # only works on picom
}
