from libqtile import qtile
from libqtile.command import lazy
from libqtile.config import Key

import os

from programs import programs
from Qmin import qmin
from functions import fullscreen_screenshot, swap_screens, window_to_other_screen


spr = "mod4"

keybinds = {
    
    "Qtile Options": [

        Key([spr, "shift"], "r",
            lazy.restart(),
            desc="Restart Qtile"
        ),
        Key([spr, "shift"], "q", 
            lazy.shutdown(), 
            desc="Shutdown Qtile"
        )
    ],

    "Launch Options": [

        Key([spr], "Return",
            lazy.spawn(programs["terminal"]),
            desc="Launch terminal"
        ),

        Key([spr], "r",
            lazy.spawn(programs["launcher"]),
            desc="Launch launcher"
        ),

        Key([spr], "i",
            lazy.spawn(programs["browser"]),
            desc="Launch browser"
        ),

        Key([], "Print",
            lazy.spawn(programs["screenshot"]),
            desc="Select area to screenshot"
        ),

        Key(["shift"], "Print",
            fullscreen_screenshot(),
            desc="Take screenshot of the entire screen"
        )
    ],
    
    "Monitor Controls": [
            
        Key([spr], "Tab",
            lazy.next_screen(),
            desc="Move focus to next screen"
        ),

        Key([spr, "shift"], "Tab",
            swap_screens(),
            desc="Swap all windows between screens"
        ),

        Key([spr], "s",
            window_to_other_screen(),
            desc="Move window to next screen"
        )
    ],

    "Window Controls": [
                    
        Key([spr], "w",
            lazy.window.kill(),
            desc="Kill active window"
        ),

        Key([spr], "h",
            lazy.layout.left(),
            desc="Move focus left"
        ),

        Key([spr], "j",
            lazy.layout.down(),
            desc="Move focus down"
        ),

        Key([spr], "k",
            lazy.layout.up(),
            desc="Move focus up"
        ),

        Key([spr], "l",
            lazy.layout.right(),
            desc="Move focus right"
        ),

        # Key([spr], "space",
        #     lazy.layout.next(),
        #     desc="Move focus to next window"
        # )

        Key([spr, "shift"], "h",
            lazy.layout.shuffle_left(),
            desc="Swap with window to the left"
        ),

        Key([spr, "shift"], "j",
            lazy.layout.shuffle_down(),
            lazy.layout.section_down(),
            desc="Swap with window below"
        ),
        
        Key([spr, "shift"], "k",
            lazy.layout.shuffle_up(),
            lazy.layout.section_up(),
            desc="Swap with window above"
        ),

        Key([spr, "shift"], "l",
            lazy.layout.shuffle_right(),
            desc="Swap with window to the right"
        ),

        Key([spr], "m",
            lazy.layout.maximize(),
            desc="toggle window between minimum and maximum sizes"
        ),

        Key([spr], "n", 
            lazy.layout.reset(), 
            desc="Reset all window sizes"
        ),

        Key([spr], "p",
            lazy.window.toggle_minimize(),
            lazy.group.next_window(),
            desc="Minimize window"
        ),

        Key([spr, "shift"], "p",
            qmin(),
            desc="Unminimize window(s)"
        ),

        Key([spr, "shift"], "f",
            lazy.window.toggle_floating(),
            desc="Toggle floating"
        ),

        Key([spr], "f",
            lazy.window.toggle_fullscreen(),
            desc="Toggle fullscreen"
        )
    ],

    "Multimedia Controls": [
        Key([], "XF86AudioStop",
            lazy.spawn("playerctl stop"),
            desc="Stop media playback"
        ),

        Key([], "XF86AudioPrev",
            lazy.spawn("playerctl previous"),
            desc="Previous track"
        ),

        Key([], "XF86AudioPlay",
            lazy.spawn("playerctl play-pause"),
            desc="Pause/continue media playback"
        ),

        Key([], "XF86AudioNext",
            lazy.spawn("playerctl next"),
            desc="Next track"
        ),

        Key([], "XF86AudioMute",
            lazy.spawn("pactl set-sink-mute @DEFAULT_SINK@ toggle"),
            desc="(Un)mute audio"
        ),

        Key([], "XF86AudioLowerVolume",
            lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ -10%"),
            desc="Lower volume by 10% (in dB)"
        ),

        Key([], "XF86AudioRaiseVolume",
            lazy.spawn("pactl set-sink-volume @DEFAULT_SINK@ +10%"),
            desc="Raise volume by 10% (in dB)"
        ),

    ]
}
