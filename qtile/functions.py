import os
import subprocess
from libqtile.command import lazy

def get_mouse_battery():
    def get_icon(percentage, icon_dict):
        closest = max(x for x in icon_dict.keys() if x <= percentage)
        return icon_dict[closest]

    try:
        return_text = (
            subprocess.check_output(
                [os.path.expanduser("~/.local/bin/rivalcfg"), "--battery-level"],
                stderr=subprocess.DEVNULL)
                .decode("utf-8")
                .replace("[", "")
                .replace("]", "")
                .replace("=", "")
                .split(" ")
        )
    except subprocess.CalledProcessError:
        return ""
    
    try:
        while True:
            return_text.remove("")
    except ValueError:
        pass
    
    state = return_text[0]
    percentage = int(return_text[1])
    charge_icons = {
        0: "", # same as 20, to keep script from crashing
        20: "",
        30: "",
        40: "",
        60: "",
        80: "",
        90: "",
        100: ""
    }
    discharge_icons = {
        0: "",
        10: "",
        20: "",
        30: "",
        40: "",
        50: "",
        60: "",
        70: "",
        80: "",
        90: "",
        100: ""
    }
    if state == "Charging":
        icon = get_icon(percentage, charge_icons)
    else:
        icon = get_icon(percentage, discharge_icons)

    display_text = f"{icon} {percentage}%"
    last_return = display_text
    return display_text

@lazy.function
def window_to_other_screen(qtile):
    i = 0 if qtile.screens.index(qtile.current_screen) else 1
    group = qtile.screens[i].group.name
    qtile.current_window.togroup(group, switch_group=False)
    qtile.cmd_to_screen(i)


@lazy.function
def swap_screens(qtile):
    i = "2" if qtile.current_screen.group.name == "1" else "1"
    qtile.current_screen.cmd_toggle_group(i)


@lazy.function
def fullscreen_screenshot(qtile):
    origin = 1920 if qtile.current_screen.cmd_info()["index"] == 0 else 0
    coords = f"{origin},0,1920,1080"
    command = os.path.expandvars(f"scrot -a {coords} \"$HOME/Desktop/Screenshots/%F_%T_%s.png\"")
    os.system(command)
