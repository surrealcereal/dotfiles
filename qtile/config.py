from libqtile.config import Screen
from libqtile.bar import Bar

# * imports define values qtile expects to exist. Imports specified by name are user-defined objects unless specified otherwise.

from keybinds import keybinds
from floating_rules import *
from toggles import *
from theme import wallpaper, colors
from widgets import init_widgets
from widgets import widget_defaults # Builtin
from layouts import *
from groups import *
from hooks import *

if __name__ in ["config", "__main__"]:
    wmname = "Qtile"
    keys = [j for i in keybinds.values() for j in i]
    screens = [
        Screen(top=Bar(widgets=init_widgets(), size=20, border_width=5, opacity=0.8, background=colors["transparent"], border_color=colors["transparent"]), wallpaper=wallpaper),
        Screen(wallpaper=wallpaper)
    ]
