from libqtile import layout

from theme import colors

layout_theme = {
    "border_width": 1,
    "margin": 0,
    "border_focus": colors["text"],
    "border_normal": "#272727"
}

layouts = [
    layout.MonadTall(**layout_theme)
]
