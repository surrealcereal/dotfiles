import os

programs = {
    "terminal": "alacritty",
    "browser":  "firefox",
    "launcher": "rofi -show drun",
    "screenshot": os.path.expandvars("scrot -s -z \"$HOME/Desktop/Screenshots/%F_%T_%s.png\"")
}

