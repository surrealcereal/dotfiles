import os
from libqtile import hook

last_client_id = None
last_client = None

@hook.subscribe.client_focus
def track_focused(current_client):
    global last_client_id, last_client
    # Check if a last_client exists, check if the current_client is different and is in the same
    # group, i.e. on the same screen, so that switching between screens does not trigger unmaximize.
    if last_client_id and last_client_id != current_client.wid and last_client._group and last_client._group.name == current_client._group.name:
        last_client.cmd_disable_fullscreen()
    last_client_id = current_client.wid
    last_client = current_client


@hook.subscribe.startup_once
def autostart():
    rc = os.path.expanduser('~/.config/qtile/qtilerc')
    os.system(rc)
