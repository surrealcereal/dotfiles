from libqtile import layout

floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules,
])
