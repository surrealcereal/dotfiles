import subprocess
import os
from qtile_extras import widget
import libqtile.bar

from theme import colors
from functions import get_mouse_battery

widget_defaults = {
    "font": "SauceCodePro Nerd Font Medium",
    "fontsize": 11,
    "background": colors["transparent"],
    "foreground": colors["text"]
}

default_decorations = [ 
    widget.decorations.RectDecoration(
            colour = colors["background"],
            radius = 10, 
            filled = True,
        )
]

interwidget_seperator = widget.Sep(linewidth = 0, padding = 5)

def init_widgets():
    widgets = [
        widget.Clock(
            padding = 10,
            decorations = default_decorations,
            format = " %H:%M"
        ),

        interwidget_seperator,

        widget.Clock(
            padding = 10,
            decorations = default_decorations,
            # format = "  %a, %d %b"
            format = "%a, %d %b"
        ),
        
        widget.Spacer(
            libqtile.bar.STRETCH,
        ),
    
        widget.GenPollText(
            padding = 10,
            decorations = default_decorations,
            func = get_mouse_battery,
            update_interval = 1
        )    
    ]
    return widgets
