set nu
set relativenumber
set nohlsearch
set hidden
set noerrorbells
set smartindent
set noswapfile
set tabstop=4
set shiftwidth=4
set incsearch
set scrolloff=8

set ffs=unix

" Insert newline without entering insert mode
nmap <C-j> o<Esc>k
nmap <C-k> O<Esc>j

" Disable arrow keys
cnoremap <Down> <Nop>
cnoremap <Left> <Nop>
cnoremap <Right> <Nop>
cnoremap <Up> <Nop>

inoremap <Down> <Nop>
inoremap <Left> <Nop>
inoremap <Right> <Nop>
inoremap <Up> <Nop>

nnoremap <Down> <Nop>
nnoremap <Left> <Nop>
nnoremap <Right> <Nop>
nnoremap <Up> <Nop>

vnoremap <Down> <Nop>
vnoremap <Left> <Nop>
vnoremap <Right> <Nop>
vnoremap <Up> <Nop>

" Disable ex mode
nnoremap Q <Nop>

" Do not clear clipboard when leaving vim
autocmd VimLeave * call system('echo ' . shellescape(getreg('+')) . 
            \ ' | xclip -selection clipboard')

call plug#begin()

Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}

call plug#end()
let g:mkdp_auto_start = 1
