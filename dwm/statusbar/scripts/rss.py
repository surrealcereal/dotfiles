#!/usr/bin/python

import os
import pickle
import feedparser
import time
from commons import get_colors


def print_empty():
    print(f"^b#282828^^c#282828^")


def extend_path(path):
    return os.path.expandvars(f"$XDG_RUNTIME_DIR/rss/{path}")


def create_rss_dir():
    rss_dir = os.path.expandvars("$XDG_RUNTIME_DIR/rss")
    try:
        os.mkdir(rss_dir)
    except FileExistsError:
        pass


def handle_content():
    # It is a concious choice to refresh content in the middle of displaying it, as if this were
    # not to be done until all news are finished, we would be reading very old news by the time it
    # got to refresh, and refreshing without any interruption to the user experience, through e.g.
    # adding the news to the end of the currently displaying news item, would introduce unnecessary
    # complexity to the script.

    # TODO: add said complexity, add currently displaying item, found from pickled files, to the
    # beginning of new flow and calculate index to start at to preserve contiuity.
    TOLERANCE = 60 * 60  # 1 hour

    pickle_file = extend_path("pickle")
    index_file = extend_path("index")
    force_hydration = False

    try:
        m_time = os.path.getmtime(pickle_file)
    except FileNotFoundError:
        create_if_nonexistant(pickle_file, "wb", bytes())
        m_time = 0

    pickle_age = time.time() - m_time
    try:
        chunks, links = read_pickled_content()
    except EOFError:  # the pickle file was just created
        force_hydration = True

    last_i = get_index(raw=True)

    if force_hydration or pickle_age > TOLERANCE or len(chunks) < last_i:
        chunks, links = hydrate_content()
        create_pickle(chunks, links)

    return chunks, links


def read_pickled_content():
    # The content, which consists of the raw text and the links, is bundled up into a single object
    # to make pickling and unpickling easier. If we were to have a seperate object for each during
    # pickling, that would mean introducing repetitive lines, or using eval() to assign to them.
    # They are split into their own objects afterwards anyways.

    with open(extend_path("pickle"), "rb") as f:
        # The seperation into individual objects is handled at the main() level.
        return pickle.load(f)


def hydrate_content():
    # This function both gets the segments of text to print and the links belonging to said
    # segments, as the creation and flattening of the segments is the only point at time where this
    # info can be obtained.

    feed = feedparser.parse("https://haber.sol.org.tr/rss.xml")
    while not feed.entries:
        feed = feedparser.parse("https://haber.sol.org.tr/rss.xml")

    chunk_size = 20
    seperator = " | "

    # Start the text stream with two spaces, as one scrolls past immidiately. This has the unwanted
    # side effect of looking bad when: 1. hitting the backwards scrolling limit, or, 2. wrapping
    # around from the end of the stream, since the first space that is usually not shown due to
    # being during the initialization of the widget is now shown, as the script has already 
    # initialized.
    text = ["  "]
    links = dict()

    for i in feed.entries:
        text.append(i.title)
        text.append(seperator)

        # Record the ending index of each title so that this information can be later used to follow
        # links.
        links[len("".join(text))] = i.id

    text.pop()  # remove last seperator
    # add space to the end of stream so that it doesn't immidiately skip to index 0, making it
    # impossible to read
    text.append(" ")
    text_str = "".join(text)
    chunks = [text_str[i: i + chunk_size] for i in range(len(text_str))]

    # Remove all elements that are smaller than chunk size, which end up at the end of the text when
    # it has to scroll to completion. This ends up being the last chunk_size - 1 elements, and doing
    # lst[:-n] removes the last n elements, and, -(chunk_size - 1) == -chunk_size + 1.
    chunks = chunks[:-chunk_size + 1]

    return chunks, links


def create_if_nonexistant(path, mode, init_content):
    if not os.path.exists(path):
        with open(path, mode) as f:
            f.write(init_content)


def create_pickle(chunks, links):
    content = [chunks, links]
    pickle_file = extend_path("pickle")
    with open(pickle_file, "wb") as f:
        pickle.dump(content, f)


def check_delay():
    # See set_delay() for info.
    delay_file = extend_path("delay")
    create_if_nonexistant(delay_file, "w", "0")
    with open(delay_file, "r") as f:
        return int(f.read()) > 0


def overwrite(path, content):
    with open(path, "w") as f:
        f.write(content)


def get_index(chunks=list(), raw=False):
    index_file = extend_path("index")
    create_if_nonexistant(index_file, "w", "0")

    with open(index_file, "r") as f:
        last_i = int(f.read())
    
    if raw:
        return last_i

    delay = check_delay()
    if delay:
        i = last_i
        set_delay("decrement")

    else:
        i = last_i + 1
        try:  # do not exceed bounds in either direction
            chunks[i]
        except IndexError:
            i = 0
            

    overwrite(index_file, str(i))
    return i


def print_output(out, BG1, FG, BG2):
    TS = " "
    # 4-4, 24px, +8
    print(f"{BG1}{FG}{TS}{TS}{TS}{TS}{TS}{TS}{TS}{TS}{BG2}{out} ")


def set_delay(mode, n=0):
    # This is a system that implements a delay between the user scrolling back/forth through the
    # news by making the script display the same output for n iterations of the script, so for
    # n * refresh_frequency seconds. This makes it less jarring to scroll around, especially
    # backwards, as without such a system, the user has to actively counteract the natural scrolling
    # of the text with their scrolling, which does not make for a good experience.
    delay_file = extend_path("delay")
    if mode == "overwrite":
        overwrite(delay_file, str(n))
    elif mode == "decrement":
        with open(delay_file, "r") as f:
            remaining_delay = int(f.read())
        overwrite(delay_file, str(remaining_delay - 1))
    else:
        raise ValueError(f"Unrecognized mode for set_delay: {mode}")


def handle_clicks(chunks, links, i):
    def shift_i(i=0, n=0, no_delta=False):
        index_file = extend_path("index")
        if no_delta:
            to_write = n
        else:
            to_write = i + n

        # Do not allow scrolling past behind 0 or beyond the boundries of the text.
        if to_write < 0:
            to_write = 0
        if to_write > len(chunks) - 1:  
            # This is not set to 0 but len(chunks) because setting it to 0 means the user can
            # accidentally scroll past the end with no means of getting back.
            # len(chunks) - 1 since length is 1-indexed but indices are 0-indexed.
            to_write = len(chunks) - 1

        overwrite(index_file, str(to_write))
        set_delay("overwrite", n=2)

    def get_current_news_item(i, links):
        # Locate current link by checking the entry in links with the smallest entry that is bigger
        # than the current index, i.e. the entry whose end index has not been reached yet, or the
        # entry that is currently displaying.
        return min([entry for entry in links.keys() if i < entry])

    button = os.getenv("BLOCK_BUTTON")
    scroll_shift = 2

    if button == "1":  # left click
        end_index = get_current_news_item(i, links)
        # This is done with os.system to bypass a bug in UtkarshVerma/dwmblocks-async, which is what
        # I use. See: https://github.com/UtkarshVerma/dwmblocks-async/issues/24
        # If you do not have this problem, feel free to do it properly:
        # import subprocess
        # subprocess.Popen(["firefox", "--new-tab", links[end_index]])
        os.system(f"firefox --new-tab {links[end_index]} >/dev/null 2>&1 &")

    elif button == "4":  # scroll up
        shift_i(i=i, n=scroll_shift)
    elif button == "5":  # scroll down
        # -1 to mitigate the flow of the text and make the speeds of of scrolling equally fast
        # bidirectionally
        shift_i(i=i, n=-scroll_shift - 1)
    elif button == "3":  # right click
        end_index = get_current_news_item(i, links)
        # -2 to make prompt display the seperator before continuing, so that it is clear that the
        # last news item was skipped.

        # This will cause immidiate left clicks after using this functionality to follow through to
        # the last news item, since the seperator is still on the screen, and this is not a bug, but
        # a feature.
        shift_i(n=end_index - 1, no_delta=True)


def main():
    create_rss_dir()
    chunks, links = handle_content()
    i = get_index(chunks=chunks)
    BG1, FG, BG2 = get_colors()
    print_output(chunks[i], BG1, FG, BG2)
    handle_clicks(chunks, links, i)


if __name__ == "__main__":
    main()
