#!/usr/bin/python
import os

BG1 = "^b#b16286^"
FG = "^c#1d2021^"
BG2 = "^b#d3869b^"

def print_output(chunks):
        TS=" "
        print(f"{BG1}{FG}{TS}{TS}{TS}{TS}{TS}{TS}{TS}{TS}{BG2}{chunks} ") # 4-4, 24px, +8

print_output("Click Me")
if os.getenv("BLOCK_BUTTON"):
    os.system(f"firefox --new-tab example.com")
    # subprocess.Popen(["firefox", "--new-tab", links[closest_i]])
