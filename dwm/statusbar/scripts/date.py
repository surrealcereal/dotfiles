#!/usr/bin/python

import datetime
from commons import get_colors


def get_date(BG1, FG, BG2):
    fmtstring = "%a, %d/%m/%y"
    date = datetime.datetime.now().strftime(fmtstring)
    TS = " "
    HS = " "
    icon = ""
    return f"{BG1}{FG}{TS}{TS}{HS}{HS}{icon}{TS}{TS}{TS}{TS}{TS}{HS}{BG2} {date} "


def main():
    BG1, FG, BG2 = get_colors()
    print(get_date(BG1, FG, BG2))


if __name__ == "__main__":
    main()
