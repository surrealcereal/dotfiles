#!/usr/bin/python

import subprocess
import os
from commons import get_colors


def get_mouse_battery(BG1, FG, BG2):
    def get_icon(percentage, icon_dict):
        closest = max(x for x in icon_dict.keys() if x <= percentage)
        return icon_dict[closest]

    def format(BG1, FG, BG2, per=0, icon="", connected=True):
        # See slstatus/config.h for an explanation regarding TS and HS and the notation used in the 
        # comments proceeding the returns.
        HS = " "
        TS = " "
        if connected:
            return f"{BG1}{FG}{TS}{TS}{TS}{TS}{icon} {HS}{BG2} {per}% "  # 9-9, 25px, -1
        else:
            return f"{BG1}{FG}{TS}{TS}{TS}{HS} {HS}{HS}"  # 8-8, 25px, +3

    try:
        return_text = (
            subprocess.check_output(
                [os.path.expanduser("~/.local/bin/rivalcfg"),
                 "--battery-level"],
                stderr=subprocess.DEVNULL)
            .decode("utf-8")
            .replace("[", "")
            .replace("]", "")
            .replace("=", "")
            .split(" ")
        )
    except subprocess.CalledProcessError:
        return format(BG1, FG, BG2, connected=False)

    try:
        while True:
            return_text.remove("")
    except ValueError:
        pass

    state = return_text[0]
    if state == "Unable":
        return format(BG1, FG, BG2, connected=False)

    percentage = int(return_text[1])
    charge_icons = {
        0: "",  # same as 20, to keep script from crashing
        20: "",
        30: "",
        40: "",
        60: "",
        80: "",
        90: "",
        100: ""
    }
    discharge_icons = {
        0: "",
        10: "",
        20: "",
        30: "",
        40: "",
        50: "",
        60: "",
        70: "",
        80: "",
        90: "",
        100: ""
    }
    if state == "Charging":
        icon = get_icon(percentage, charge_icons)
    else:
        icon = get_icon(percentage, discharge_icons)

    return format(BG1, FG, BG2, per=percentage, icon=icon)


def main():
    BG1, FG, BG2 = get_colors()
    print(get_mouse_battery(BG1, FG, BG2))


if __name__ == "__main__":
    main()
