#define CMDLENGTH 10000
#define DELIMITER "^b#282828^^c#282828^ "
#define CLICKABLE_BLOCKS
#define PATH(name)	"$HOME/.config/dwm/statusbar/scripts/"name

const Block blocks[] = {
	BLOCK(PATH("rss.py") 			" ^b#b16286^ ^c#1d2021^ ^b#d3869b^", 1, 1),
	BLOCK(PATH("mouse_battery.py") 	" ^b#d79921^ ^c#1d2021^ ^b#fabd2f^", 1, 0),
	BLOCK(PATH("date.py") 			" ^b#689d6a^ ^c#1d2021^ ^b#8ec07c^", 1, 0),
	BLOCK(PATH("time.py") 			" ^b#cd241d^ ^c#1d2021^ ^b#fb4934^", 1, 0),
};
