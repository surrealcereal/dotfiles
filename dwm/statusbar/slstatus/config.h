/* See LICENSE file for copyright and license details. */

/* interval between updates (in ms) */
const unsigned int interval = 1000;

/* text to show if no value can be retrieved */
static const char unknown_str[] = "n/a";

/* maximum output string length */
#define MAXLEN 2048

/*
 * function            description                     argument (example)
 *
 * battery_perc        battery percentage              battery name (BAT0)
 *                                                     NULL on OpenBSD/FreeBSD
 * battery_state       battery charging state          battery name (BAT0)
 *                                                     NULL on OpenBSD/FreeBSD
 * battery_remaining   battery remaining HH:MM         battery name (BAT0)
 *                                                     NULL on OpenBSD/FreeBSD
 * cpu_perc            cpu usage in percent            NULL
 * cpu_freq            cpu frequency in MHz            NULL
 * datetime            date and time                   format string (%F %T)
 * disk_free           free disk space in GB           mountpoint path (/)
 * disk_perc           disk usage in percent           mountpoint path (/)
 * disk_total          total disk space in GB          mountpoint path (/")
 * disk_used           used disk space in GB           mountpoint path (/)
 * entropy             available entropy               NULL
 * gid                 GID of current user             NULL
 * hostname            hostname                        NULL
 * ipv4                IPv4 address                    interface name (eth0)
 * ipv6                IPv6 address                    interface name (eth0)
 * kernel_release      `uname -r`                      NULL
 * keyboard_indicators caps/num lock indicators        format string (c?n?)
 *                                                     see keyboard_indicators.c
 * keymap              layout (variant) of current     NULL
 *                     keymap
 * load_avg            load average                    NULL
 * netspeed_rx         receive network speed           interface name (wlan0)
 * netspeed_tx         transfer network speed          interface name (wlan0)
 * num_files           number of files in a directory  path
 *                                                     (/home/foo/Inbox/cur)
 * ram_free            free memory in GB               NULL
 * ram_perc            memory usage in percent         NULL
 * ram_total           total memory size in GB         NULL
 * ram_used            used memory in GB               NULL
 * run_command         custom shell command            command (echo foo)
 * swap_free           free swap in GB                 NULL
 * swap_perc           swap usage in percent           NULL
 * swap_total          total swap size in GB           NULL
 * swap_used           used swap in GB                 NULL
 * temp                temperature in degree celsius   sensor file
 *                                                     (/sys/class/thermal/...)
 *                                                     NULL on OpenBSD
 *                                                     thermal zone on FreeBSD
 *                                                     (tz0, tz1, etc.)
 * uid                 UID of current user             NULL
 * uptime              system uptime                   NULL
 * username            username of current user        NULL
 * vol_perc            OSS/ALSA volume in percent      mixer file (/dev/mixer)
 *                                                     NULL on OpenBSD/FreeBSD
 * wifi_perc           WiFi signal in percent          interface name (wlan0)
 * wifi_essid          WiFi ESSID                      interface name (wlan0)
 */
#define SEP { run_command,	"%s",		"echo \'^b#282828^^c#282828^ \'" }
static const struct arg args[] = {
	// Icons in the nerd-font set are not centered. This means they will have an offset to either
	// direction naturally when placed inside a block and this offset needs to be adjusted for with
	// exotic spaces that are more fine-grained than a regular space.
	//
	// The two spaces used here are the thin space (U+2009) and the hair space (U+200A).
	//
	// The notation of the form at+bh - ct+dh denotes how many of these spaces are placed to each
	// side of the icon, shown at the right and left of the "-" seperator respectively.
	//
	// The notation of the forn x-x, ypx, +/-z denotes the gap on either side of the icon when the
	// space configuration in this config file is used (x), the total size of the block (ypx) and
	// the natural offset the icon has (+/-z). 
	//
	// This	is calculated by placing the icon in between two normal spaces and deducting the number of 
	// pixels to the right from the number of pixels to the left, i.e. by calculating its offset from
	// the center position.

	// 4-4, 24px, +8
	// 2t - 6t
	// { run_command,	"^b#b16286^^c#1d2021^        ^b#d3869b^ %s",		"python $HOME/.config/dwm/slstatus/components/rss.py" },
	// { run_command,	"%s", "python $HOME/.config/dwm/slstatus/components/rss.py ^b#b16286^ ^c#1d2021^ ^b#d3869b^" },
	{ run_command,	"%s", "python $HOME/.config/dwm/slstatus/components/rss.py ^b#b16286^ ^c#1d2021^ ^b#d3869b^" },
	SEP,
	{ run_command,	"%s", "python $HOME/.config/dwm/slstatus/components/mouse_battery.py ^b#d79921^ ^c#1d2021^ ^b#fabd2f^" },
	SEP,
	// 6-6, 25px, +5
	// 2t+2h - 5t+h
	{ datetime,	"%s",		"^b#689d6a^^c#1d2021^          ^b#8ec07c^ %a, %d/%m/%y " },
	SEP,
	// 6-6, 24px, +4
	// 2t+2h - 5t
	{ datetime, "%s",		"^b#cd241d^^c#1d2021^         ^b#fb4934^ %H.%M " },
	// the color of the clock icon bg is supposed to be cc241d, however this displays as such on my
	// machine
	 
	/*
	Classic look, matches default dwm colors 
	bg, fg: ^b#005577^^c#eeeeee^
    sep: ^b#26282a
	*/
	
};
