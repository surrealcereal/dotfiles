import os
import feedparser

feed = feedparser.parse("https://haber.sol.org.tr/rss.xml")

chunk_size = 20
sep = " · "
text = list()
for i in feed.entries:
    text.append(i.title)    
    text.append(sep)

text_str = "".join(text)
chunks = [text_str[i:i+chunk_size] for i in range(len(text_str))]

with open(os.path.expandvars("$HOME/test"), "r+") as f:
    last_i = int(f.read())
    i = last_i + 1
    try:
        chunks[i]
    except IndexError:
        i = 0
    print(chunks[i])
    f.seek(0)
    f.write(str(i))

