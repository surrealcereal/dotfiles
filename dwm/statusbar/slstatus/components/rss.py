import os
import sys
import feedparser

TS=" "
ZWJ="‍"

def get_rss(bg1, fg, bg2):
    feed = feedparser.parse("https://haber.sol.org.tr/rss.xml")
    chunk_size = 20
    sep = " | "
    text = list()
    if not feed.entries:
        return f"^b#282828^^c#282828^"

    for i in feed.entries:
        text.append(i.title)    
        text.append(sep)

    text_str = "".join(text)
    chunks = [text_str[i:i+chunk_size] for i in range(len(text_str))]

    path = os.path.expandvars("$XDG_RUNTIME_DIR/rss_index")

    if not os.path.exists(path):
        with open(path, "w") as f:
            f.write("0")
    with open(path, "r+") as f:
        last_i = int(f.read())
        i = last_i + 1
        try:
            chunks[i]
        except IndexError:
            i = 0
        f.seek(0)
        f.write(str(i))
        return f"{bg1}{fg}{TS}{TS}{TS}{TS}{TS}{TS}{TS}{TS}{bg2}{chunks[i]} " # 4-4, 24px, +8

def main():
    print(get_rss(sys.argv[1], sys.argv[2], sys.argv[3]))

if __name__ == "__main__":
    main()

