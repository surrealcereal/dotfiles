export EDITOR="/usr/bin/vim"

export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.local/state"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_RUNTIME_DIR="/run/user/$UID"

export HISTFILE="$XDG_STATE_HOME/bash/history"
export LESSHISTFILE="$XDG_CACHE_HOME/less/history"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/pythonrc"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export VIMINIT='let $MYVIMRC = !has("nvim") ? "$XDG_CONFIG_HOME/vim/vimrc" : "$XDG_CONFIG_HOME/nvim/init.vim" | so $MYVIMRC'
export _JAVA_OPTIONS="-Djava.util.prefs.userRoot=$XDG_CONFIG_HOME/java"
export CARGO_HOME="$XDG_DATA_HOME"/cargo

export WINIT_X11_SCALE_FACTOR=1 alacritty
export GPG_TTY=$TTY

export PATH="$PATH:$HOME/.local/bin:$HOME/.local/share/cargo/bin"

if [[ "$(tty)" = "/dev/tty1" ]]; then
	pgrep dwm || sx "$HOME/.config/X11/xinitrc" &>/dev/null # only call sx if qtile is not running elsewhere
fi
